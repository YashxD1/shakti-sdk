## Guide
  * [Introduction](#introduction)
  * [Board Information](#board-information)
  * [Buying Board](#buying-board)
  * [Vendor Documentation](#vendor-documentation)
  
### Introduction ###

Shakti-sdk aims to provide support for different types of boards. As part of this plan, initially we support two variety of FPGA boards. <br/>
Artix 7 35T board corresponds to E class and Artix 7 100T corresponds to C class. E class of processors are 32 bit cores and C class are 64 bit cores.<br/>
The readme, includes information on installing Vivado, building the bitsream. [board_use.md](https://gitlab.com/shaktiproject/software/shakti-sdk/blob/master/doc/board_use.md) provides information on connecting a JTAG, programming the on-board configuration memory and running example shakti-sdk projects.

### Board information ###
Details on board support for different classes of processors are given below

- 32 bit core
  * [`shakti e class`](https://shakti.org.in/e-class.html)supported on [`artix7_35t board`](https://www.xilinx.com/products/boards-and-kits/arty.html).
- 64 bit core
  * [`shakti c class`](https://shakti.org.in/c-class.html) supported on [`artix7_100t board`](https://www.xilinx.com/products/boards-and-kits/1-w51quh.html).

### Buying Board  ###
  - [Digilent](https://store.digilentinc.com/arty-a7-artix-7-fpga-development-board-for-makers-and-hobbyists/)
  - [Amazon](https://www.amazon.in/Digilent-Artix-7-Development-Makers-Hobbyists/dp/B017BOBNEO?tag=googinhydr18418-21)
  
### Vendor Documentation ###

   - [`Xilinx - Artix-7 FPGA`](https://www.xilinx.com/products/silicon-devices/fpga/artix-7.html/) 
   - [`Xilinx - Vivado Design Suite`](https://www.xilinx.com/products/design-tools/vivado.html/) 
   - [`Digilent - Arty A7: Artix7 FPGA Development Board for Makers and Hobbyists`](https://store.digilentinc.com/arty-a7-artix-7-fpga-development-board-for-makers-and-hobbyists/)

